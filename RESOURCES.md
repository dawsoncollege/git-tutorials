# Some, maybe useful, resources

## Cheat sheets & guides

* [Rebellabs cheat sheet](https://zeroturnaround.com/wp-content/uploads/2016/02/Git-Cheat-Sheet.png)   Thanks to Aline & Ken 
* [NDP Soft cheat sheet](http://ndpsoftware.com/git-cheatsheet.html)
* [simple guide](http://rogerdudler.github.io/git-guide/)

## Interactive Tutorials

* [basic concepts git tutorial](https://try.github.io/levels/1/challenges/1)  (there are others that are paid)
* [git branching tutorial](https://learngitbranching.js.org/)

## Troubleshooting and misc

* [git flight rules](https://github.com/k88hudson/git-flight-rules)  Thanks to  Maja
* [markdown cheat sheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
* [differences between github/gitlab/bitbucket](https://about.gitlab.com/2016/01/27/comparing-terms-gitlab-github-bitbucket/)
* [Choosing a license](https://opensource.guide/legal/) 

## Understanding git

* [git from the inside out](https://mayrosecook.com/blog/post/git-from-the-inside-out)

## Books

* [Git Pro (2014)](https://git-scm.com/book/en/v2)  Thanks to Ken

## Github classroom
* [for teachers](https://github.com/jfiksel/github-classroom-for-teachers)
* [for students](https://github.com/jfiksel/github-classroom-for-students)
